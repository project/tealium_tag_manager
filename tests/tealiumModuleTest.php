<?php
/**
 * @file
 * Tealium Module Tests.
 */

/**
 * PHPunit Tealium.
 *
 * To run :
 *
 * phpunit --coverage-clover reports/clover.xml --coverage-html reports/html
 * --log-junit reports/junit.xml -c profiles/b2bprofile/modules/shared/tealium/phpunit.xml.dist
 *
 * @group Tealium
 */
class TeaiumModuleTest extends \PHPUnit_Framework_TestCase {

  /**
   * Test valid Tealium values.
   *
   * @dataProvider getValidTealiumValues
   */
  public function testValidTealiumValue($value) {
    $result = tealium_is_valid_value($value);
    $this->assertTrue($result);
  }

  /**
   * Test valid Tealium values.
   *
   * @dataProvider getInvalidTealiumValues
   */
  public function testInvalidTealiumValue($value) {
    $result = tealium_is_valid_value($value);
    $this->assertFalse($result);
  }

  /**
   * Valid values for Tealium.
   */
  function getValidTealiumValues() {
    return array(
      array(1),
      array(234),
      array(99999999),
      array(0),
      array("0"),
      array('0'),
      array('front'),
      array('TAGVALUE'),
      array('TAG VALUE WITH SPACES'),
      array('<tags_in_values>'),
      array('}{|"@)*(&*&'),
      array('?><Jjsdgf^%@£!£44232231¡€€¡#¢1'),
    );
  }

  /**
   * Invalid values for Tealium.
   */
  function getInvalidTealiumValues() {
    return array(
      // No empty string accepted.
      array(''),

      // Not int or string, so fail.
      array(NULL),
      array(FALSE),
      array(TRUE),
      array(array()),
      array(array('foo' => 'bar')),
      array(array('foo' => 'bar', 'foo2' => 'bar2')),
      array(new \stdClass()),
      array(new \Exception()),
      array(new \Exception('Mou')),
    );
  }

  /**
   * Test encoding arrays.
   *
   * @dataProvider tealiumEncodeValuesData
   */
  public function testTealiumEncodeValues($value, $expected) {
    $result = tealium_encode_values($value);

    $this->assertEquals($result, $expected);
  }

  /**
   * Invalid values for Tealium.
   */
  function tealiumEncodeValuesData() {

    return array(
      array(
        'value' => array(
          'foo' => 'bar',
          // testing repeated values.
          'foo' => 'bar',
        ),
        'expected' => '"foo":"bar"',
      ),

      array(
        'value' => array(
          'pageName' => 'home',
          'pageTitle' => 'A cool title',
        ),
        'expected' => '"pageName":"home", "pageTitle":"A cool title"',
      ),

      // Multiple dimension arrays.
      array(
        'value' => array(
          'pageName' => 'home',
          'productID' => array(
            'pageName' => 'home',
            'pageTitle' => 'home',
          )
        ),
        'expected' => '"pageName":"home", "productID":{"pageName":"home", "pageTitle":"home"}',
      ),

      array(
        'value' => array(
          'pageName' => 'home',
          'productID' => array(
            'pageName' => 'home',
            'subProductID' => array(
              'pageTitle' => 'home',
            )
          )
        ),
        'expected' => '"pageName":"home", "productID":{"pageName":"home", "subProductID":{"pageTitle":"home"}}',
      ),

      array(
        'value' => array(
          'pageName' => 'home',
          'productID' => array(
            'pageName' => 'home',
            'subProductID' => array(
              'pageTitle' => 'home',
            )
          )
        ),
        'expected' => '"pageName":"home", "productID":{"pageName":"home", "subProductID":{"pageTitle":"home"}}',
      ),

      array(
        'value' => array(
          'pageName' => 'home',
          'productID' => array(
            'pageName' => 'home',
            'subProductID' => array(
              'pageTitle' => 'home',
            ),
            'subsubProductID' => array(
              'pageTitle' => 'home',
              'subSubSubProductID' => array(
                'pageTitle' => 'home',
              ),
              'pageName' => 'home',
            ),
          )
        ),
        'expected' =>
          '"pageName":"home", "productID":{"pageName":"home", "subProductID":{"pageTitle":"home"}, "subsubProductID":{"pageTitle":"home", "subSubSubProductID":{"pageTitle":"home"}, "pageName":"home"}}',
      ),

    );
  }

  /**
   * Test encoding arrays.
   *
   * @dataProvider tealiumEncodeValuesWrongData
   */
  public function testWrongTealiumEncodeValues($value, $expected) {
    $result = tealium_encode_values($value);

    $this->assertEquals($result, $expected);
  }


  /**
   * Invalid values for Tealium.
   */
  function tealiumEncodeValuesWrongData() {

    return array(
      array(
        'value' => NULL,
        'expected' => '',
      ),

      array(
        'value' => array(),
        'expected' => '',
      ),

      array(
        'value' => array(''),
        'expected' => '"0":""',
      ),

      array(
        'value' => 'wrong value',
        'expected' => '',
      ),

      array(
        'value' => 0,
        'expected' => '',
      ),

      array(
        'value' => 2340,
        'expected' => '',
      ),

    );
  }
}
